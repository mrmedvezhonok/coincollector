import 'package:coins_collector/view_model/list_coins_journal_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'journal_datatable_view.dart';

class CoinsJournalScreenView extends StatefulWidget {
  CoinsJournalScreenView({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _CoinsJournalScreenViewState createState() => _CoinsJournalScreenViewState();
}

class _CoinsJournalScreenViewState extends State<CoinsJournalScreenView> {

  @override
  void initState() {
    super.initState();
    final listCoinsJournal =  Provider.of<ListCoinsJournalViewModel>(context, listen: false);
    listCoinsJournal.fetchCoinsRows();
  }

  @override
  Widget build(BuildContext context) {
    final listCoinsJournal =  Provider.of<ListCoinsJournalViewModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        automaticallyImplyLeading: true,
      ),
     body: CoinsJournalDataTableView(coinsList: listCoinsJournal.coins,),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Navigator.pushNamed(context, '/add');
          print('back');

        },
        child: const Icon(Icons.add),
    )
    );
  }
}
