import 'package:coins_collector/view_model/coin_view_model.dart';
import 'package:flutter/material.dart';

class CoinsJournalDataTableView extends StatelessWidget {

  const CoinsJournalDataTableView({Key? key, required this.coinsList}) : super(key: key);

  final List<CoinViewModel> coinsList;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: DataTable(
        sortColumnIndex: 0,
        sortAscending: true,
        columns: [
          DataColumn(label: Text("Denomination"), numeric: true),
          DataColumn(label: Text("Year"), numeric: true),
        ],
        // rows: [],
        rows: coinsList.map((coinViewModel) => DataRow(
          cells: [
            DataCell(Text(coinViewModel.coin.denomination.toString())),
            DataCell(Text(coinViewModel.coin.year.toString())),
          ],
        )).toList(),
      ),
    );
  }
}
