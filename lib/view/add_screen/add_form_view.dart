import 'package:coins_collector/model/data/Coin.dart';
import 'package:coins_collector/view_model/list_coins_journal_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddFormView extends StatefulWidget {
  const AddFormView({Key? key}) : super(key: key);

  @override
  _AddFormViewState createState() => _AddFormViewState();
}

class _AddFormViewState extends State<AddFormView> {

  final _formKey = GlobalKey<FormState>();
  final Coin coin = new Coin();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                icon: Icon(Icons.attach_money),
                hintText: 'Value of coin',
                labelText: 'Denomintion',
              ),
              onSaved: (String? value) {
                if (value == null) throw Exception('Denomination value is necessary!');
                this.coin.denomination = int.parse(value);
              },
              validator: (String? value) {
                RegExp exp = RegExp(r'^\d{1,3}$');
                return (value == null || exp.allMatches(value).length == 0) ?
                "Denomination is required" : null;
              },
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                icon: Icon(Icons.calendar_today),
                hintText: 'Year of production of the coin',
                labelText: 'Year',
              ),
              onSaved: (String? value) {
                if (value == null) throw Exception('Year value is necessary!');
                this.coin.year = int.parse(value);
              },
              validator: (String? value) {
                RegExp exp1 = RegExp(r'^199\d$');
                RegExp exp2 = RegExp(r'^20[012]\d$');
                return value == null? "Year is required" :
                (exp1.allMatches(value).length == 0 && exp2.allMatches(value).length == 0)?
                "Year of coin must be 20th or 21st century" : null;

              },
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: ElevatedButton(
                onPressed: () {
                  if(_formKey.currentState!.validate()) {
                    _formKey.currentState!.save();

                    int denomination = this.coin.denomination!;
                    int year = this.coin.year!;

                    final listCoinsJournal = Provider.of<ListCoinsJournalViewModel>(context, listen: false);
                    listCoinsJournal.insertCoin(coin);
                    listCoinsJournal.fetchCoinsRows();

                    ScaffoldMessenger.of(context)
                        .showSnackBar(
                        SnackBar(content: Text('Added coin $denomination of $year year'))
                    );
                    Navigator.pop(context);
                  }
                },
                child: Text('Submit'),
              ),
            )
          ],
        )
      )
    );
  }
}


