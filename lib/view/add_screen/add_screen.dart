import 'package:coins_collector/view/add_screen/add_form_view.dart';
import 'package:flutter/material.dart';

class AddScreenView extends StatelessWidget {
  const AddScreenView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add a coin'),
      ),
      body: AddFormView(),
    );
  }
}



