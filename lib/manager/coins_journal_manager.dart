import 'package:coins_collector/model/db/mcoin.dart';
import 'package:coins_collector/provider/sqlite_connection_provider.dart';
import 'package:sqflite/sqflite.dart';

class CoinsJournalManager {

  Future<List<MCoin>> fetchCoins() async {
    final dbConnection = await DbConnectionManager.getConnection();
    final list = await dbConnection.rawQuery('SELECT * FROM coins ORDER BY denomination, year');

    final List<MCoin> coins = [];
    list.forEach((e) => coins.add(MCoin.fromMap(e)));

    return coins;
  }

  Future<void> insertCoin(MCoin coin) async {
    var dbConnection = await DbConnectionManager.getConnection();

    await dbConnection.insert(
        'coins',
        coin.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace
    );
  }
}