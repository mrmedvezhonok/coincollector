class Coin{

  Coin({this.id, this.denomination, this.year});

  int? id;
  int? denomination;
  int? year;
}