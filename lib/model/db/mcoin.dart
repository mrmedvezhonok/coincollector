import 'package:coins_collector/model/db/model.dart';

class MCoin extends Model{
  MCoin({
    required int id,
    required this.denomination,
    required this.year
  }) : super(id);

  final int year;

  final int denomination;

  factory MCoin.fromMap(Map<String, dynamic> m) => MCoin(
    id: m['id'],
    denomination: m['denomination'],
    year: m['year'],

  );

  @override
  Map<String, dynamic> toMap() => {
    // 'id': id,
    'denomination': denomination,
    'year': year,
  };

  @override
  String toString() {
    return 'MCoin{id: $id, denomination: $denomination, year: $year}';
  }

}