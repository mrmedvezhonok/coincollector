import 'package:coins_collector/core/entry.dart';
import 'package:coins_collector/model/data/Coin.dart';
import 'package:coins_collector/model/db/mcoin.dart';
import 'package:flutter/material.dart';

import 'coin_view_model.dart';

class ListCoinsJournalViewModel with ChangeNotifier {
  List<CoinViewModel> coins = <CoinViewModel>[];
  final entry = Entry();

  Future<void> fetchCoinsRows() async {
    final result = await entry.coinsJournal.fetchCoins();
    this.coins = result.map((coin) => CoinViewModel(coin: coin)).toList();
    notifyListeners();
  }

  Future<void> insertCoin(Coin coin) async {
    final mcoin = MCoin(id: 0, denomination: coin.denomination!, year: coin.year!);
    await entry.coinsJournal.insertCoin(mcoin);
  }
}
