import 'package:coins_collector/view/add_screen/add_screen.dart';
import 'package:coins_collector/view/journal_screen/journal_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'view_model/list_coins_journal_view_model.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      // DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight
      DeviceOrientation.portraitUp
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ListCoinsJournalViewModel())
      ],
      child:  MaterialApp(
        title: 'Coins',
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        initialRoute: '/home',
        routes: {
          '/home': (context) => CoinsJournalScreenView(title: 'Coins journal'),
          '/add': (context) => AddScreenView(),
        }
      )
    );
  }
}
