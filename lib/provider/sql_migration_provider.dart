import 'dart:async';

import 'package:flutter/services.dart';

class SqlMigrationProvider {
  static const migrationsDir = 'migrations';

  // static Future<String> get _localPath async {
  //   final directory = await getApplicationDocumentsDirectory();
  //   return join(directory.path, migrationsDir);
  // }
  //
  // static Future<File> get _createMigrationFile async {
  //   final path = await _localPath;
  //   return File(join(path, migrationsDir));
  // }


  static Future<String> get createMigrationText async {
    try {
      final content = await rootBundle.loadString('assets/migrations/create.sql');
      return content;
    } catch (e) {

      return '';
    }
  }

}