import 'package:coins_collector/provider/sql_migration_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';


class DbConnectionManager {
  static Database? _connection;

  static Future<Database> _openConnection() async {
    const dbName = 'coins_collection.db';
    final createMigrationQuery = await SqlMigrationProvider.createMigrationText;
    final dbConnection = await openDatabase(
      join(await getDatabasesPath(), dbName),
      onCreate: (Database db, int version) async {
        await db.execute(createMigrationQuery);
      },
      version: 1,
    );
    return dbConnection;
  }

  static getConnection() async {
    if (_connection == null) {
      _connection = await _openConnection();
    }
    return _connection;
  }
}

